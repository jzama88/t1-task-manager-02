package com.t1.alieva.tm.constant;

public class TerminalConst {

    public final static String VERSION = "version";

    public final static String HELP = "help";

    public final static String ABOUT = "about";

}
